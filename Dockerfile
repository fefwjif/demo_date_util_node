FROM node:14
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

HEALTHCHECK --interval=10s --timeout=5s \
  CMD curl -fs http://localhost/ || exit 1

EXPOSE 3000
CMD [ "npm", "start" ]