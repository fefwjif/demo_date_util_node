const express = require('express');
const router = express.Router();
const logger = require('../logger');
const moment = require('moment');
const moment_tz = require("moment-timezone");
moment_tz.tz.setDefault('Asia/Shanghai');

router.get('/', function(req, res, next) {
  logger.info(req.originalUrl)

  let p = req.query
  let result = {}
  if (p.hasOwnProperty('getGameRoundTable')){
      let gameRoundId = parseInt(p.getGameRoundTable)
    if ((isNaN(gameRoundId) == true)) {
      res.status(500).json("getGameRoundTable value is NaN");
    }
      result['GameRoundTable'] = {
        "Table":Number((((gameRoundId/86400)%1024).toFixed(0))),
        sql:`SELECT * FROM ab_gaming.t_gameround_${((gameRoundId/86400)%1024).toFixed(0)} tg;`
      }

  }

  if (p.hasOwnProperty('getClientGameHallBal')){
    let clientId = parseInt(p.getClientGameHallBal)
    if ((isNaN(clientId) == true)) {
      res.status(500).json("getClientGameHallBal value is NaN");
    }
    result['ClientGameHallBal'] = {
          "Table":Number((clientId%1024).toFixed(0)),
          sql: `SELECT * FROM ab_gaming.t_client_gamehall_bal_${(clientId%1024).toFixed(0)} tcgb;`
        }
  }

  if (p.hasOwnProperty('getClientCreditLog')){
    let logTime = parseInt(p.getClientCreditLog)
    if ((isNaN(logTime) == true)) {
      res.status(500).json("getClientCreditLog value is NaN");
    }
    let r = dateProvider(logTime)
    result['ClientCreditLog'] = {
      "Table":Number(r),
      sql: `SELECT * FROM ab_agent_system.t_client_credit_log_${r} tccl;`
    }
  }

  if (p.hasOwnProperty('getClientBetLog')){
    let clientId = parseInt(p.getClientBetLog)
    if ((isNaN(clientId) == true)) {
      res.status(500).json("getClientBetLog value is NaN");
    }
    result['ClientBetLog'] = {
      "Table":Number((clientId%1024).toFixed(0)),
      sql: `SELECT * FROM ab_gaming.t_client_bet_log_${(clientId%1024).toFixed(0)} tcbl;`
    }
  }

  res.json(result)
});

router.get("/getClientCreditLogFromTimeStamp", function(req, res, next) {
  logger.info(req.originalUrl)

  if (req.query.timeStamp === undefined){
    throw new Error("must containe query: timeStamp")
  }
  
  let date = new Date(parseInt(req.query.timeStamp))
  let r = dateProvider(req.query.timeStamp)

  res.json({
    "table":r,
    Date: "Date: "+ date.getDate()+
    "/"+(date.getMonth()+1)+
    "/"+date.getFullYear()+
    " "+date.getHours()+
    ":"+date.getMinutes()+
    ":"+date.getSeconds(),
    sql: `SELECT * FROM ab_agent_system.t_client_credit_log_${r} tccl ORDER BY tccl.log_time DESC;`
  })
});

router.get("/getClientCreditLogFromDateTime", function(req, res, next) {
  logger.info(req.originalUrl)

  if (req.query.dateTime === undefined){
    throw new Error("must containe query: dateTime")
  }

  const date = moment(req.query.dateTime, 'YYYY-MM-DD HH:mm:ss').toDate();

  let r = dateProvider(date.getTime())

  res.json({
    "table":r,
    Date: "Date: "+ moment(date).format('YYYY-MM-DD HH:mm:ss'),
    sql: `SELECT * FROM ab_agent_system.t_client_credit_log_${r} tccl ORDER BY tccl.log_time DESC;`
  })
});

router.get("/getTimeRangeTables", function(req, res, next) {
  logger.info(req.originalUrl)

  let moment_date

  if (req.query.timeRange === undefined){
    moment_date = moment(new Date().getTime())
  }else{
    try {
      moment_date = moment(req.query.timeRange, 'YYYY-MM-DD')
    }catch(err){
      throw new Error(err.message)
    }
  }

  let dateObj = {}

  for (let index = 0; index < moment_date.daysInMonth(); index++) {
    setMomentDate(moment_date, index);
    dateObj[`Date: ${moment_date.format('YYYY-MM-DD')}`] = `SELECT * FROM ab_agent_system.t_client_credit_log_${dateProvider(moment_date.toDate().getTime())} tccl ORDER BY tccl.log_time DESC;`
  }

  res.json({
    Date: "Date: "+ moment(moment_date).format('YYYY-MM-DD'),  
    "table":dateObj
  })
});

function dateProvider(d){
  let n = d - 1262318400000
  let t = (((n/1000)%1000000000)-43200).toFixed(0)
  return ((t/86400)%1024).toFixed(0)
}

function setMomentDate(moment_date, index){
  moment_date.set('date', 1 + index)
  moment_date.set('hour', 12);
  moment_date.set('minute', 00);
  moment_date.set('second', 00);
  moment_date.set('millisecond', 000);
}

module.exports = router;