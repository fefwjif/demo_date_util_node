var express = require('express');
var router = express.Router();
const logger = require('../logger');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.status(200).json({ user: 'geek' })
});

module.exports = router;
