const { createLogger, format, transports, winston} = require('winston');
const { combine, timestamp, label, prettyPrint } = format;


const logger = createLogger({
  format: combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.splat(),
    format.printf(info => `${info.timestamp} : ${info.message}`)
  ),
  exitOnError: false,
  transports: [new transports.Console({}),
    new transports.File({ filename: 'log/log.log'}),
    //new transports.File({ filename: 'log/error_log.log'})
  ]
})

// const elogger = createLogger({
//   level: 'debug',
//   format: format.combine(
//     format.errors({ stack: true }),
//   ),
//   transports: [new transports.Console(),
//      new transports.File({ filename: 'error_log.log' })],
// });

module.exports =  logger;